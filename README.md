# OpenML dataset: tae

https://www.openml.org/d/48

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown -   
**Please cite**:   

1. Title: Teaching Assistant Evaluation
 
 2. Sources:
    (a) Collector: Wei-Yin Loh (Department of Statistics, UW-Madison)
    (b) Donor:     Tjen-Sien Lim (limt@stat.wisc.edu)
    (b) Date:      June 7, 1997
 
 3. Past Usage:
    1. Loh, W.-Y. & Shih, Y.-S. (1997). Split Selection Methods for 
       Classification Trees, Statistica Sinica 7: 815-840.
    2. Lim, T.-S., Loh, W.-Y. & Shih, Y.-S. (1999). A Comparison of
       Prediction Accuracy, Complexity, and Training Time of
       Thirty-three Old and New Classification Algorithms. Machine
       Learning. Forthcoming.
       (ftp://ftp.stat.wisc.edu/pub/loh/treeprogs/quest1.7/mach1317.pdf or
       (http://www.stat.wisc.edu/~limt/mach1317.pdf)
 
 4. Relevant Information:
    The data consist of evaluations of teaching performance over three
    regular semesters and two summer semesters of 151 teaching assistant
    (TA) assignments at the Statistics Department of the University of
    Wisconsin-Madison. The scores were divided into 3 roughly equal-sized
    categories ("low", "medium", and "high") to form the class variable.
 
 5. Number of Instances: 151
 
 6. Number of Attributes: 6 (including the class attribute)
 
 7. Attribute Information:
   
    1. Whether of not the TA is a native English speaker (binary)
       1=English speaker, 2=non-English speaker
    2. Course instructor (categorical, 25 categories)
    3. Course (categorical, 26 categories)
    4. Summer or regular semester (binary) 1=Summer, 2=Regular
    5. Class size (numerical)
    6. Class attribute (categorical) 1=Low, 2=Medium, 3=High
 
 8. Missing Attribute Values: None

 Information about the dataset
 CLASSTYPE: nominal
 CLASSINDEX: last

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/48) of an [OpenML dataset](https://www.openml.org/d/48). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/48/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/48/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/48/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

